##### Choee Park

### How to build install and use your project:
1. It is built with C#.NET. 
2. Download all the source files and the directories to your local PC.
3. Open the DormAndMealPlan.sln file with Visual Studio. 
4. Compile and launch the program. 

### Why I chose the license:
It is a simple and permissive license as this project was intented for academic use rather than commercial use. 