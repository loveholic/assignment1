﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DormAndMealPlan
{
    public class Dormitory
    {
        public string DormName { get; set; }
        public decimal DormPrice { get; set; }

        public Dormitory()
        { }

        public Dormitory(string dormName, decimal dormPrice)
        {
            DormName = dormName;
            DormPrice = dormPrice;
        }

        public override string ToString()
        {
            return $"{DormName} (${DormPrice}/semester)";
        }

    }
}
