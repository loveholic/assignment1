﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DormAndMealPlan
{
    public class MealPlan
    {
        public string MealPlanName { get; set; }
        public decimal MealPlanPrice { get; set; }

        public MealPlan()
        {
        }

        public MealPlan(string mealPlanName, decimal mealPlanPrice)
        {
            MealPlanName = mealPlanName;
            MealPlanPrice = mealPlanPrice;
        }

        public override string ToString()
        {
            return $"{MealPlanName}  (${MealPlanPrice}/semester)";
        }

    }

   
    


}
