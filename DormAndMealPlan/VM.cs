﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DormAndMealPlan
{
    public class VM : INotifyPropertyChanged
    {
        public BindingList<Dormitory> Dormitories { get; set; } = new BindingList<Dormitory>();
        public BindingList<MealPlan> MealPlans { get; set; } = new BindingList<MealPlan>();

        public Dormitory SelectedDormitory { get; set; }
        public MealPlan SelectedMealPlan { get; set; }

        public int selectedDormitoryIdx = 0;
        public int selectedMealPlanIdx = 0;
        public string selectedDormatoryPriceOutput = "";
        public string selectedMealPlanPriceOutput = "";
        public string totalCharge = "";

        public int SelectedDormitoryIdx
        {
            get => selectedDormitoryIdx;
            set { selectedDormitoryIdx = value; notifyChange(); }
        }

        public int SelectedMealPlanIdx
        {
            get => selectedMealPlanIdx;
            set { selectedMealPlanIdx = value; notifyChange(); }
        }
        public string SelectedDormatoryPriceOutput
        {
            get => selectedDormatoryPriceOutput;
            set { selectedDormatoryPriceOutput = value; notifyChange(); }
        }
        public string SelectedMealPlanPriceOutput
        {
            get => selectedMealPlanPriceOutput;
            set { selectedMealPlanPriceOutput = value; notifyChange(); }
        }
        public string TotalCharge
        {
            get => totalCharge;
            set { totalCharge = value; notifyChange(); }
        }
        public VM()
        {
            Dormitory dAllen = new Dormitory();
            dAllen.DormName = "Allen Hall";
            dAllen.DormPrice = 1500m;
            Dormitories.Add(dAllen);

            Dormitory dPike = new Dormitory();
            dPike.DormName = "Pike Hall";
            dPike.DormPrice = 1600m;
            Dormitories.Add(dPike);

            //Option 1 
            Dormitory dFarthing = new Dormitory();
            dFarthing.DormName = "Farthing Hall";
            dFarthing.DormPrice = 1800m;
            Dormitories.Add(dFarthing);

            //Option 2
            Dormitory dUSuites = new Dormitory();
            dUSuites.DormName = "University Suites";
            dUSuites.DormPrice = 2500m;
            Dormitories.Add(dUSuites);

            //Option 3, If I want to use the variable names: dormName, dormPrice
            string dormName = "University Suites";
            decimal dormPrice = 2500m;
            Dormitory dUsuites = new Dormitory(dormName, dormPrice);

            MealPlan mSeven = new MealPlan();
            mSeven.MealPlanName = "7 Meals/week";
            mSeven.MealPlanPrice = 600m;
            MealPlans.Add(mSeven);

            MealPlan mFourteen = new MealPlan();
            mFourteen.MealPlanName = "14 Meals/week";
            mFourteen.MealPlanPrice = 1200m;
            MealPlans.Add(mFourteen);

            MealPlan mUnlimited = new MealPlan();
            mUnlimited.MealPlanName = "Unlimited Meals";
            mUnlimited.MealPlanPrice = 1700m;
            MealPlans.Add(mUnlimited);
        }

        public void CalculateTotal()
        {
            selectedDormatoryPriceOutput = "$" + SelectedDormitory.DormPrice.ToString();
            selectedMealPlanPriceOutput = "$" + SelectedMealPlan.MealPlanPrice.ToString();
            totalCharge = "$" + (SelectedDormitory.DormPrice + SelectedMealPlan.MealPlanPrice).ToString();

            notifyChange("TotalCharge");
            notifyChange("SelectedDormatoryPriceOutput");
            notifyChange("SelectedMealPlanPriceOutput");
        }

        #region PropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        private void notifyChange([CallerMemberName]string property = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        #endregion
    }
}
