﻿using System.Windows;

namespace DormAndMealPlan
{
    /// <summary>
    /// Interaction logic for Total.xaml
    /// </summary>
    public partial class Total : Window
    {
        public Total(VM vm)
        {
            InitializeComponent();
            DataContext = vm;
        }
    }
}
