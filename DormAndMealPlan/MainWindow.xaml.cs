﻿using System;
using System.Windows;

namespace DormAndMealPlan
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public VM vm = new VM();
        Total total;
        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
            
        }

        private void BtnCalculate_Click(object sender, RoutedEventArgs e)
        {
            if (total == null) // Restricts the second window to open only once
            {
                total = new Total(vm);
                total.Owner = this;
                total.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                total.Closed += Total_Closed;
                total.Show();
            }

            vm.CalculateTotal();
        }
        private void Total_Closed(object sender, EventArgs e)
        {
            total = null;
        }
    }
}
